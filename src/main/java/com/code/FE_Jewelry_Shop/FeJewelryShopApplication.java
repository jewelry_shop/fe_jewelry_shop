package com.code.FE_Jewelry_Shop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FeJewelryShopApplication {

	public static void main(String[] args) {
		SpringApplication.run(FeJewelryShopApplication.class, args);
	}

}
